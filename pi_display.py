import sys
from random import choice, randrange
from string import ascii_letters
from mpmath import mp
import pygame as pg

pindex=0
digits=4000
mp.dps = digits  # set number of digits
a=str(mp.pi)


def blit_text(surface, text, pos, font, color=pg.Color('black')):
    global pindex
    space = font.size(' ')[0]  # The width of a space.
    max_width, max_height = surface.get_size()
    x, y = pos
    for char in range(len(text)):
        char_surface = font.render(text[char], 0, color)
        char_width, char_height = char_surface.get_size()
        if x + char_width >= max_width:
            x = pos[0]  # Reset the x.
            y += char_height  # Start on new row.
        surface.blit(char_surface, (x, y))
        x += char_width + space
        if y >= (max_height - char_height):
            pindex=1
            break
        
def main():
    global pindex
    info = pg.display.Info()
    screen = pg.display.set_mode((info.current_w, info.current_h), pg.FULLSCREEN)
    screen_rect = screen.get_rect()
    font = pg.font.Font(None, 45)
    font2 = pg.font.Font(None,120)
    clock = pg.time.Clock()
    color = (randrange(256), randrange(256), randrange(256))
    txt = font.render("PI STARTS!!", True, color)
    txtinit = font.render("PI STARTS!!", True, color)
    timer = 10
    done = False

    while not done:
        for event in pg.event.get():
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_ESCAPE:
                    done = True

        timer -= 1
        # Update the text surface and color every 10 frames.
        if timer <= 0:
            timer = 1
            color = (randrange(256), randrange(256), randrange(256))
            txt=a[0:pindex]
            pindex=pindex+1
            if pindex==digits:
                pindex=1

            screen.fill((30, 30, 30))
            if pindex<=500:
                blit_text(screen, "PI STARTS!!", (100, 100), font2, color)
            else:
                blit_text(screen, a[0:(pindex-500)], (0,0), font, color)
                  
        pg.display.flip()
        clock.tick(3000)


if __name__ == '__main__':
    pg.init()
    main()
    pg.quit()
    sys.exit()
